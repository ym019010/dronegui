package JavaGUI;

/**
 * 
 * @author Robert P
 *
 */
public class Obstacle extends Objects {
	protected int id;
	private static int begin = 1;
	int speed = 0;
	int angle = 0;
	/**
	 * instantiates it all
	 * @param x
	 * @param y
	 */
	public Obstacle(double x, double y){
		super(x, y);
		size = 5;
		colour = 'g';
		id = begin++;
	}
	public String toString(){
		return "Obstacle " + id + " is at (" + xPos + "," +yPos + ")";
		
	}
	public void tryToMove(DroneArena a){	
	}
	
	public void setId(int givenID){
		id = givenID;
	}
	public String saveObstacle() {
        String saveData = "";
        saveData += colour + "\n";
        saveData += id + "\n";
        saveData += xPos + "\n";
        saveData += yPos + "\n";

        saveData += speed + "\n";
        saveData += angle;
        return saveData;
    }


	
}

