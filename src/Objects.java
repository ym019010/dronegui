package JavaGUI;

/**
 * 
 * @author Robert P
 *
 */
public abstract class Objects {
	double xPos;
	double yPos;
	
	int size;
	char colour; 
	/**
	 * instantiates it all
	 * @param x
	 * @param y
	 */
	public Objects(double x, double y){
		xPos = x;
		yPos = y;
		
	}
	/**
	 * getter
	 * @return  x position of object
	 */
	public double getX(){
		return xPos;
	}
	/**
	 * getter 
	 * @return y position of object
	 */
	public double getY(){
		return yPos;
	}
	/**
	 * 
	 */
	public abstract String toString();
	

	public abstract void tryToMove(DroneArena a);

	/**
	 * getter
	 * @return size of object
	 */
	public int getSize(){
		return size;
		}
	/**
	 * getter
	 * @return colour
	 */
	public char getCol(){
		return colour;
	}
	/**
	 * checks if object is at specific location
	 * @param x
	 * @param y
	 * @return true or false depending on space
	 */
	public boolean isHere(double x, double y){
		double top = y + size;
		double right = x + size ;
		double bottom = y - size ;
		double left = x - size;
		
		if((xPos < right && xPos > left) && (yPos < top && yPos > bottom)){
			return true;
		}
		return false;
	}
	
	/**
	 * displays object in arena
	 * @param c
	 */
	public void doDisplay(MyCanvas c){
		c.displayCircle(xPos,yPos, size, colour);
	}
	
	
	
}

