package JavaGUI;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ListView;

import javax.swing.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * @author Robert P
 *
 */
public class DroneInterface extends Application {
	int canvasSize = 512;
	MyCanvas mc;
	DroneArena arena;
	VBox rtPane;
	boolean animation = false;
	
	/**
	 * shows where objects are and their info
	 */
	public void drawInfo(){
		rtPane.getChildren().clear();
		Label l = new Label(arena .toString());
		rtPane.getChildren().add(l);
	}
	
	/**
	 * draws the arena
	 */
	public void displaySystem(){
		mc.clearCanvas();
		arena.showDrone(mc);
		drawInfo();
	}
	/**
	 * Shows a message
	 * @param Title
	 * @param Content 
	 */
	public void showMessage(String Title, String Content){
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(Title);
		alert.setHeaderText(null);
		alert.setContentText(Content);
	}
	/**
	 * sets up the buttons so you can change the simulation
	 * @return
	 */
	private HBox setButtons(){
		//starts animation
		 Button btnAniOn = new Button("Start Simulation");
		 btnAniOn.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent event) {
	                animation = true;
	            }
	        });
		 //stops animation
		 Button btnAniOff = new Button("Stop Simulation");
		 btnAniOff.setOnAction(new EventHandler<ActionEvent>() {
	            
	            public void handle(ActionEvent event) {
	                animation = false;
	            }
	        });
		 // adds drone
		 Button btnNewD = new Button("New Drone");  
		 btnNewD.setOnAction(new EventHandler<ActionEvent>(){
			 public void handle(ActionEvent event){
				 arena.addDrone('d');
				 displaySystem();
			 }
		 });
		 //adds obstacle
		 Button btnNewO = new Button("New Obstacle");
	        // now add handler
	        btnNewO.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent event) {
	                arena.addDrone('o');
	                displaySystem();
	            }
	        });
	        // adds Death Drone
	        Button btnNewDD = new Button("New Death Drone");
	        // now add handler
	        btnNewDD.setOnAction(new EventHandler<ActionEvent>() {
	         
	            public void handle(ActionEvent event) {
	                arena.addDrone('D');
	                displaySystem();
	            }
	        });

	        return new HBox(btnAniOn, btnAniOff, btnNewD, btnNewO, btnNewDD);
	    }
		
	/**
	 * shows the about info in the menu
	 */
	public void showAbout(){
		showMessage("About", "Robert's Drone Simulation");
	}
	
	/**
	 * loads a file from the directory
	 */
	public void openFile() throws IOException{
		Thread thr = new Thread(new Runnable() {
            @Override
            public void run() {
                JFileChooser selector = new JFileChooser("D:\\Computer Science\\Part 2\\Java\\Drone Sim\\Java\\GUIFile");
                int validate = selector.showOpenDialog(null);
                if (validate == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = selector.getSelectedFile();
                    if (selectedFile.isFile()) {
                        try {
                            String path = selector.getSelectedFile().getPath();
                            Path filePath = Paths.get(path);
                            long lineCount = Files.lines(filePath).count();
                            double xPos = 0;
                            double yPos = 0;
                            int ID = 0;
                            char colour = 'd';
                            double speed = 0;
                            double angle = 0;

                            int arenaSizeX = Integer.parseInt(Files.readAllLines(filePath).get(0), 10);
                            int arenaSizeY = Integer.parseInt(Files.readAllLines(filePath).get(1), 10);
                            arena = new DroneArena(arenaSizeX, arenaSizeY);
                            for (int i = 2; i < lineCount; i = i + 6) {
                                colour = Files.readAllLines(filePath).get(i).charAt(0);
                                ID = Integer.parseInt(Files.readAllLines(filePath).get(i + 1), 10);
                                xPos = Double.parseDouble(Files.readAllLines(filePath).get(i + 2));
                                yPos = Double.parseDouble(Files.readAllLines(filePath).get(i + 3));
                                speed = Double.parseDouble(Files.readAllLines(filePath).get(i + 4));
                                angle = Double.parseDouble(Files.readAllLines(filePath).get(i + 5));
                                arena.addObj(colour, xPos, yPos, ID, speed, angle);
                            }
                            displaySystem();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
               }
            }
        });
        thr.start();
    }
		
	/**
	 * sets up the menu
	 * @return
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();
		Menu mHelp = new Menu("Help");
		MenuItem mAbt = new Menu("About");
		mAbt.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent ae) {
				showAbout();
			}
			
		});
		mHelp.getItems().addAll(mAbt);
		
		Menu mFile = new Menu("File");
		MenuItem mExit =  new Menu("Exit");
		 mExit.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent t) {
	                System.exit(0);                        
	            }
	        });

		 MenuItem mSave = new MenuItem("Save");
	        mSave.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent t) {
	                try {
						arena.saveArena();
					} catch (IOException e) {
						
						e.printStackTrace();
					}                        
	            }
	        });
	        
	     MenuItem mLoad = new MenuItem("Load Arena");
	        mLoad.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent t) {
	                try {
						openFile();
					} catch (IOException e) {
						e.printStackTrace();
					}                        
	            }
	        });
	        
	        mFile.getItems().addAll(mSave, mLoad, mExit);
	        menuBar.getMenus().addAll(mFile, mHelp);
	        
	        return menuBar;
  

	}
	/** 
	 * starts the whole process
	 */
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("YM019010");
		
		BorderPane bp = new BorderPane();
		
		bp.setTop(setMenu());
				
		Group root = new Group();
		Canvas canvas = new Canvas(canvasSize,canvasSize);
		root.getChildren().add(canvas);
		mc = new MyCanvas(canvas, canvasSize, canvasSize);
		arena = new DroneArena(canvasSize, canvasSize);
		
		bp.setCenter(root);
		
		rtPane = new VBox();
		bp.setRight(rtPane);
		
		Scene scene = new Scene(bp, canvasSize * 1.6, canvasSize * 1.2);
		
		new AnimationTimer(){
			public void handle(long currentNanoTime){
				if(animation){
					arena.moveAllDrones(mc); 
					displaySystem();
				}
			}
		}.start();
		
		bp.setBottom(setButtons());
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
		
	
	public static void main(String[] args){
		Application.launch(args);
		}
}
