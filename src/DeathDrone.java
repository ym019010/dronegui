package JavaGUI;
/**
 * 
 * @author Robert P
 *
 */
public class DeathDrone extends Drone{
	static protected int begin = 1;
	int id;
	/**
	 * instantiates deathdrone
	 * @param x
	 * @param y
	 */
	public DeathDrone(double x, double y){
		super(x,y);
		speed = 1;
		size = 9;
		colour = 'r';
		start--;
		id = begin++;
		
	}
	/**
	 * prints information about death drone
	 */
	public String toString(){
		return "Death Drone" + id + " is at (" + String.format("%.2f", xPos) + "," + String.format("%.2f", yPos) + ")" +" heading at angle " + String.format("%.2f", angle);

	}
	/**
	 * checks if death drone is at a specific location
	 * @return true if yes false if no
	 */
	public boolean isHere(double x, double y){
		double top = y+size*2;
        double bottom = y-size*2;
        double left = x-size*2;
        double right = x+size*2;
        
        if((xPos <= right && xPos >= left) && ( yPos <= top && yPos >= bottom)){
        	return true;
        }	
       return false;
	}
}
