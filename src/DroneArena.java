package JavaGUI;



import java.util.ArrayList;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.scene.control.ListView;


import java.util.ArrayList;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.scene.control.ListView;
import javax.swing.*;


/**
 * 
 * @author ym019010
 * The Arena where droneList are located
 */
public class DroneArena {
	public int arenaSizeX;
	public int arenaSizeY;
	public ArrayList<Drone> droneList = new ArrayList<Drone>();
	public ArrayList<Obstacle> obstacleList = new ArrayList<Obstacle>();
	public ArrayList<DeathDrone> deathList = new ArrayList<DeathDrone>();
	private Random randomGenerator;
	

	/**
	 * 	Initialises the arena with a given size 
	 * @param x
	 * @param y
	 */
	public DroneArena(int x, int y){
		arenaSizeX= x;
		arenaSizeY= y ;
		randomGenerator = new Random(); //initialises of random number
	}
	/**
	 * displays drone onto the canvas
	 * 
	 * @param c
	 */
	public void showDrone(MyCanvas c){
		for(Drone d: droneList){
			d.doDisplay(c);
		}
		for(Obstacle O: obstacleList){
			O.doDisplay(c);
		}
		for(DeathDrone D: deathList){
			D.doDisplay(c);
		}
	}
	
	
	/**
	 * adds a drone,Death drone or obstacle to the arena
	 * @param type
	 */
	public void addDrone(char type){
		double xLoc = randomGenerator.nextInt(arenaSizeX);
		double yLoc = randomGenerator.nextInt(arenaSizeY);
		boolean clash = true;
		
		while(clash){
			if(getDroneAt(xLoc, yLoc) == null){
				clash = false;
			}
			xLoc = randomGenerator.nextInt(arenaSizeX);
			yLoc = randomGenerator.nextInt(arenaSizeY);
		}
		if(type == 'd'){
			Drone newObj = new Drone(xLoc,yLoc);
			droneList.add(newObj);
		}
		if(type == 'o'){
			Obstacle newObj = new Obstacle(xLoc,yLoc);
			obstacleList.add(newObj);
		}
		if(type == 'D'){
			DeathDrone newObj = new DeathDrone(xLoc,yLoc);
			deathList.add(newObj);
		}
	}

	
	/**
	 * adds a piece from a saved arena
	 * @param col
	 * @param x
	 * @param y
	 * @param id
	 * @param spd
	 * @param deg
	 */
	public void addObj(char col, double x, double y, int id, double spd, double deg){
		if( col == 'b'){
			Drone newObj = new Drone(x,y);
        	newObj.setDirection(spd,deg);
        	newObj.setId(id);
        	droneList.add(newObj);
		}
		if( col == 'g'){
			Obstacle newObj = new Obstacle(x, y);
	        newObj.setId(id);
	        obstacleList.add(newObj);
		}
		if(col == 'r'){
			DeathDrone newObj = new DeathDrone(x,y);
            newObj.setDirection(spd,deg);
            newObj.setId(id);
            deathList.add(newObj);
		}
	}

	/**
	 * removes a regular drone if collision with death drone occurs
	 * @param mc myCanvas
	 */
	public void death(MyCanvas mc){
		 
		for(DeathDrone D: deathList){
			for(Drone d: droneList){
				if(D.isTouching(d.getX(),d.getY()))
				{
					droneList.remove(d);
						
				}
			}
		}
	}

	

	/**
	 * checks ArrayList for drones checking of drones are at their location
	 * @param x
	 * @param y
	 * @return null if there's no drone
	 */
	public Objects getDroneAt(double x, double y) {
		for(Obstacle d : obstacleList) {
			if(d instanceof Obstacle) {
				if(d.isHere(x,y)){
					return d;
				}
			}
		}
		
		for(Drone d : droneList) {
			for(DeathDrone e: deathList)
			{
				if (d.isHere(x, y))
				{
					if (e.isHere(x, y))
					{
						return e;
					}
					return d;
				}
			}
		}

        return null;
	}
	
	/**
	 * checks if space is empty for drone to move
	 * @param xCoord
	 * @param yCoord
	 * @param current
	 * @return true if you can move, false if not
	 */
	public boolean canMoveHere(double xCoord, double yCoord, Objects current){
		if(xCoord < arenaSizeX && xCoord > 0 && yCoord < arenaSizeY && yCoord > 0){
			if(getDroneAt(xCoord, yCoord)== null || getDroneAt(xCoord, yCoord) == current){
				return true;
			}
		}
		return false;
	}
	/**
	 * saves the data of the arena
	 * @return string with arena info
	 */
	public String saveArenaData() {
		 String saveData = "";
	        saveData += arenaSizeX + "\n";
	        saveData += arenaSizeY;
	        for (Drone d: droneList) {
	            saveData += "\n";
	            saveData += d.saveDrone();
	        }
	        for (DeathDrone D: deathList) {
	            saveData += "\n";
	            saveData += D.saveDrone();
	        }
	        for (Obstacle O: obstacleList) {
	            saveData += "\n";
	            saveData += O.saveObstacle();
	        }
	        return saveData;
	    }
	
	/**
	 * saves arena info to file
	 */
	public void saveArena() throws IOException {
		Thread thr = new Thread(new Runnable() {
            @Override
            public void run() {
                JFileChooser select = new JFileChooser("D:\\Computer Science\\Part 2\\Java\\Drone Sim\\Java\\GUIFile");
                int approve = select.showSaveDialog(null);
                if (approve == JFileChooser.APPROVE_OPTION) {
                    File file = select.getSelectedFile();
                    try {
                        FileWriter writeFile = new FileWriter(file);
                        PrintWriter writeHead = new PrintWriter(writeFile);
                        writeHead.println(saveArenaData());
                        writeHead.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thr.start();
    }

	
	/**
	 * 
	 * returns size of arena and drone information
	 */
	public String toString() {	
		String output = "The Arena Dimensions are: (" +	arenaSizeX + "," + arenaSizeY + ")";
		
		for (Drone d: droneList){
			output += "\n";
			output += d.toString();
		}		
		
		for(Obstacle O: obstacleList) {
			output += "\n";
			output += O.toString();
		}
		for(DeathDrone D: deathList) {
			output += "\n";
			output += D.toString();
		}
		
		return output;
	}
	/**
	 * tries to move all the drones
	 * @param a
	 */
	public void moveAllDrones(MyCanvas mc){
		for (Drone d : droneList){
			
			d.tryToMove(this);
		}
		for(DeathDrone D: deathList){
			D.tryToMove(this);
			death(mc);
		}
				
	}	
	
	public static void main(String[] args){
		DroneArena r = new DroneArena(20,10);
	}
}
