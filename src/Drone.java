package JavaGUI;

import java.util.Random;

/**
 * 
 * @author ym019010
 *
 */
public class Drone extends Objects{
	double speed = 3;
	double angle;
	double dx;
	double dy;
	static protected int start = 1;
	int id;
	
	public Drone(double x, double y){
		super(x, y);
		size = 3;
		dx = x;
		dy = y;
		colour = 'b';
		id = start++;
        Random rand = new Random(System.currentTimeMillis());
        angle = rand.nextFloat()*2*Math.PI;
	}
	
	public double getX(){
		return dx;
	}
	
	public double getY(){
		return dy;
	}
	
	public double getAng() {
		return angle;
	}
	/**
	 * 
	 * @param spd
	 * @param deg
	 */
	public void setDirection(double spd, double deg){
        speed = spd;
        angle = deg;
    }

	/**
	 * prints out information regarding Drone
	 */
	public String toString() {
		return "Drone " + id + " is at (" + String.format("%.1f", xPos) + "," + String.format("%.1f", yPos) + ") \t heading at angle " + String.format("%.1f", angle) + "\t"; 
	} 
	/**
	 * checks to see if space is empty before moving
	 * @param a
	 */
	public void tryToMove(DroneArena a){
		double newX = xPos + speed*Math.cos(angle);
		double newY = yPos + speed*Math.sin(angle);
		Random rAng = new Random();
        double randAng = 360 * rAng.nextDouble();


		
		if(a.canMoveHere(newX, newY, this)){
			xPos = newX;
			yPos = newY;
		}
		else{
			angle += Math.toRadians(randAng);
			newX = xPos + speed*Math.cos(angle);
            newY = yPos + speed*Math.sin(angle);
            xPos = newX;
            yPos = newY;
        }

	}
	/**
	 * Checks to see if objects have collided
	 * @param d
	 * @param e
	 * @return true if yes false if no
	 */
	public boolean isTouching(double d, double e){
		int radius = 20;
		if((xPos - d < radius && xPos - d > -radius) && (yPos - e < radius && yPos - e > -radius)){
			return true;	
		}
		return false;
	}
	/**
	 * sets id
	 * @param givenID
	 */
	public void setId(int givenID){
		id = givenID;
	}
	/**
	 * Saves drone data
	 * @return string with drone data
	 */
	public String saveDrone() {
        String saveData = "";
        saveData += colour + "\n";
        saveData += id + "\n";
        saveData += xPos + "\n";
        saveData += yPos + "\n";
        saveData += speed + "\n";
        saveData += angle;

        return saveData;
    }


	
}
