
package JavaGUI;



import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.geometry.VPos;

/**
 * Class to manage the canvas
 * @author Robert P
 *
 */
public class MyCanvas extends Application {
	int xCanvasSize = 512;
	int yCanvasSize = 512;
	GraphicsContext gc;
	/**
	 * constructors that sets up size and graphics of canvas
	 * @param g
	 * @param xcs
	 * @param ycs
	 */
	public MyCanvas(Canvas g, int xcs, int ycs){
		gc = g.getGraphicsContext2D();
		xCanvasSize = xcs;
		yCanvasSize = ycs;
	}
	/**
	 * getter
	 * @return arena x dimension
	 */
	public int getXCS(){
		return xCanvasSize;
	}
	/**
	 * getter
	 * @return arena y dimension
	 */
	public int getYCS(){
		return yCanvasSize;
	}

	/**
	 * sets colour, size and formats the canvas
	 * @param width
	 * @param height
	 */
	public void fillCanvas(int width, int height){
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, width , height);
		gc.setStroke(Color.BLACK);
		gc.strokeRect(0, 0, width, height);
	}
	
	/**
	 * converts a character to a colour 
	 * @param c
	 * @return
	 */
    Color chngFillCol (char c){
        Color fillCol = Color.BLACK;
        switch (c) {
        	case 'r':
        	case 'R':
        		fillCol = Color.RED;
        		break;
        	case 'b':
        	case 'B':
        		fillCol = Color.BLACK;
        		break;
        	case 'g':
        	case 'G':
        		fillCol = Color.GREEN;
        		break;
        }
        return fillCol;
    }
    /**
     * fills the colour to colour selected before
     * @param c
     */
    public void setFC(Color c){
    	gc.setFill(c);
    }
	/**
	 * shows a circle at pos x, y colour col, and radius rad
	 * @param x
	 * @param y
	 * @param rad
	 * @param col
	 */
	 public void displayCircle(double x, double y, double rad, char colour) {
	        setFC(chngFillCol(colour));
	        showCircle(x, y, rad);			
	    }
	 
	/**
	 * shows circle in the current colour at x,y and radius rad
	 * @param x
	 * @param y
	 * @param size
	 * @param colour
	 */
	public void showCircle(double x, double y,double rad) {
		gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);
		
	}
	/**
	 * shows the string 's' at location x & y
	 * @param x
	 * @param y
	 * @param s
	 */
	public void showText (double x, double y, String s){
		gc.setTextAlign(TextAlignment.CENTER);
		gc.setTextBaseline(VPos.CENTER);
		gc.setFill(Color.WHITE);
		gc.fillText(s, x, y);
	}
	/**
	 * clears the canvas
	 */
	public void clearCanvas(){
		gc.setFill(Color.AZURE);
		gc.fillRect(0,0 , xCanvasSize, yCanvasSize);
	}
	public void start(Stage arg0) throws Exception {
	}
	
}

